package sf

import (
	"github.com/julienschmidt/httprouter"
	"go.uber.org/zap"
	"io"
	"net/http"
	"sync"
)

type (
	Validator interface {
		Validate(i interface{}) error
	}

	JSONSerializer interface {
		Serialize(c Context, i interface{}, indent string) error
		Deserialize(c Context, i interface{}) error
	}

	Renderer interface {
		Render(io.Writer, string, interface{}, Context) error
	}

	Map map[string]interface{}

	Framework struct {
		router         *httprouter.Router
		pool           sync.Pool
		middleware     []MiddlewareFunc
		logger         *zap.Logger
		sugar          *zap.SugaredLogger
		Debug          bool
		ErrorHandler   HTTPErrorHandler
		Binder         Binder
		JSONSerializer JSONSerializer
		Validator      Validator
		Renderer       Renderer
	}
)

func New() *Framework {
	l, _ := zap.NewProduction()
	f := &Framework{
		router:         httprouter.New(),
		logger:         l,
		sugar:          l.Sugar(),
		ErrorHandler:   DefaultHTTPErrorHandler,
		Binder:         &DefaultBinder{},
		JSONSerializer: &DefaultJSONSerializer{},
	}

	f.router.NotFound = ErrNotFound
	f.router.MethodNotAllowed = ErrMethodNotAllowed
	f.pool.New = func() interface{} {
		return f.NewContext(nil, nil)
	}

	return f
}

func (f *Framework) Close() error {
	return f.logger.Sync()
}

func (f *Framework) Router() *httprouter.Router {
	return f.router
}

func (f *Framework) Use(m MiddlewareFunc) {
	f.middleware = append(f.middleware, m)
}

func (f *Framework) NewContext(r *http.Request, w http.ResponseWriter) *context {
	return NewContext(f, w, r)
}

// AcquireContext returns an empty `Context` instance from the pool.
// You must return the context by calling `ReleaseContext()`.
func (f *Framework) AcquireContext() Context {
	return f.pool.Get().(Context)
}

// ReleaseContext returns the `Context` instance back to the pool.
// You must call it after `AcquireContext()`.
func (f *Framework) ReleaseContext(c Context) {
	f.pool.Put(c)
}

func (f *Framework) GET(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodGet, path, handler, middleware...)
}

func (f *Framework) HEAD(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodHead, path, handler, middleware...)
}

func (f *Framework) POST(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodPost, path, handler, middleware...)
}

func (f *Framework) PUT(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodPut, path, handler, middleware...)
}

func (f *Framework) PATCH(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodPatch, path, handler, middleware...)
}

func (f *Framework) DELETE(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodDelete, path, handler, middleware...)
}

func (f *Framework) CONNECT(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodConnect, path, handler, middleware...)
}

func (f *Framework) OPTIONS(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodOptions, path, handler, middleware...)
}

func (f *Framework) TRACE(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.HandleFunc(http.MethodTrace, path, handler, middleware...)
}

func (f *Framework) HandleFunc(method string, path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	f.router.Handle(method, path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		h := applyMiddleware(handler, middleware...)
		h = applyMiddleware(h, f.middleware...)
		ctx := f.pool.Get().(*context)
		ctx.Reset(w, r)
		ctx.SetParams(p)
		ctx.SetHandler(handler)
		if err := h(ctx); err != nil {
			f.ErrorHandler(err, ctx)
		}
		f.pool.Put(ctx)
	})
}

func (f *Framework) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	f.router.ServeHTTP(w, r)
}

func (f *Framework) Log() *zap.Logger {
	return f.logger
}

func (f *Framework) Sugar() *zap.SugaredLogger {
	return f.sugar
}

func (f *Framework) ListenAndServe(addr string) error {
	return http.ListenAndServe(addr, f.router)
}

func (f *Framework) ListenAndServeTLS(addr, certFile, keyFile string) error {
	return http.ListenAndServeTLS(addr, certFile, keyFile, f.router)
}

func (f *Framework) SetNotFoundHandler(handler http.Handler) {
	f.router.NotFound = handler
}

func (f *Framework) SetMethodNotAllowedHandler(handler http.Handler) {
	f.router.MethodNotAllowed = handler
}

func applyMiddleware(h HandlerFunc, middleware ...MiddlewareFunc) HandlerFunc {
	for i := len(middleware) - 1; i >= 0; i-- {
		h = middleware[i](h)
	}
	return h
}
