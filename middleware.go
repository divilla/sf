package sf

import (
	"go.uber.org/zap"
	"net/http"
	"time"
)

type (
	HandlerFunc        func(Context) error
	MiddlewareFunc     func(HandlerFunc) HandlerFunc
	HTTPMiddlewareFunc func(HandlerFunc) http.HandlerFunc
)

func LoggerMiddleware() MiddlewareFunc {
	return func(next HandlerFunc) HandlerFunc {
		return func(ctx Context) error {
			var err error
			req := ctx.Request()
			res := ctx.Response()
			start := time.Now().UTC()
			if err = next(ctx); err != nil {
				ctx.Error(err)
			}
			dur := time.Now().UTC().Sub(start)
			path := req.URL.Path
			if path == "" {
				path = "/"
			}

			reqb := req.Header.Get(HeaderContentLength)
			if reqb == "" {
				reqb = "0"
			}

			ctx.Framework().Log().Info("request",
				zap.String("id", req.Header.Get(HeaderXRequestID)),
				zap.Time("start", start),
				zap.Int64("time_μs", dur.Microseconds()),
				zap.String("time", dur.String()),
				zap.String("ip", ctx.RealIP()),
				zap.String("host", req.Host),
				zap.String("uri", req.RequestURI),
				zap.String("method", req.Method),
				zap.String("path", path),
				zap.String("protocol", req.Proto),
				zap.String("referer", req.Referer()),
				zap.String("user_agent", req.UserAgent()),
				zap.String("request_size", reqb),
				zap.Int64("response_size", res.Size),
				zap.Int("status_code", res.Status),
				zap.String("status_text", res.StatusText()),
				zap.Error(err),
			)

			return err
		}
	}
}
